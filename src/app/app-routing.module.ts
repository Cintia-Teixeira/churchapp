import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  // {
  //   path: 'about',
  //   loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  // },
  // {
  //   path: 'preaching',
  //   loadChildren: () => import('./preaching/preaching.module').then( m => m.PreachingPageModule)
  // },
  // {
  //   path: 'creed',
  //   loadChildren: () => import('./creed/creed.module').then( m => m.CreedPageModule)
  // },
  // {
  //   path: 'activities',
  //   loadChildren: () => import('./activities/activities.module').then( m => m.ActivitiesPageModule)
  // },
  // {
  //   path: 'contact',
  //   loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  // },
  // {
  //   path: 'member-area',
  //   loadChildren: () => import('./member-area/member-area.module').then( m => m.MemberAreaPageModule)
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
