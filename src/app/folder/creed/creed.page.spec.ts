import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreedPage } from './creed.page';

describe('CreedPage', () => {
  let component: CreedPage;
  let fixture: ComponentFixture<CreedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
