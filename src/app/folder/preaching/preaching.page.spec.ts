import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreachingPage } from './preaching.page';

describe('PreachingPage', () => {
  let component: PreachingPage;
  let fixture: ComponentFixture<PreachingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreachingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreachingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
