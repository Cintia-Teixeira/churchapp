import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreachingPage } from './preaching.page';

const routes: Routes = [
  {
    path: '',
    component: PreachingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreachingPageRoutingModule {}
