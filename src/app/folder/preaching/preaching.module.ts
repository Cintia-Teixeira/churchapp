import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreachingPageRoutingModule } from './preaching-routing.module';

import { PreachingPage } from './preaching.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreachingPageRoutingModule
  ],
  declarations: [PreachingPage]
})
export class PreachingPageModule {}
