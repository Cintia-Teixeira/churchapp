import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FolderPage } from './folder.page';

const routes: Routes = [
  {
    path: '',
    component: FolderPage
  //   children: [
  //     {
  //       path: 'about',
  //       loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  //     },
  //     {
  //       path: 'preaching',
  //       loadChildren: () => import('./preaching/preaching.module').then( m => m.PreachingPageModule)
  //     },
  //     {
  //       path: 'creed',
  //       loadChildren: () => import('./creed/creed.module').then( m => m.CreedPageModule)
  //     },
  //     {
  //       path: 'activities',
  //       loadChildren: () => import('./activities/activities.module').then( m => m.ActivitiesPageModule)
  //     },
  //     {
  //       path: 'contact',
  //       loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  //     },
  //     {
  //       path: 'member-area',
  //       loadChildren: () => import('./member-area/member-area.module').then( m => m.MemberAreaPageModule)
  //     }
  //   ]
  // },
  // {
  //   path: '',
  //   redirectTo: '/folder/about',
  //   pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FolderPageRoutingModule {}
