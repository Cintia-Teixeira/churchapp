import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MemberAreaPage } from './member-area.page';

const routes: Routes = [
  {
    path: '',
    component: MemberAreaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MemberAreaPageRoutingModule {}
